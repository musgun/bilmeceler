import 'package:amenerrasulu/models/general_model.dart';
import 'package:flutter/material.dart';

import 'material_custom_color.dart';

String admobAppId = "ca-app-pub-7321558929323654~7932811121";
String admobInterstitialId = "ca-app-pub-7321558929323654/8483475466";
String admobBannerId = "ca-app-pub-7321558929323654/2399668449";

String oneSignalId = "05c52bec-bcb3-4ee2-9779-7ccbb0d12d32";
String projectName = 'Bilmeceler';

const String url =
    'https://play.google.com/store/apps/details?id=com.mgsoft.bilmeceler';

bool isSplash = false;
double fontSize = 16;
var _primaryColor = const Color(0xffFAFAFC);
var _primaryColor2 = const Color(0xffFFD402);
var _primaryColorDark = const Color(0xff231F20);
var projectColor = MaterialColor(
    _primaryColor.value, getMaterialCustomColor(_primaryColor.value));

var projectColor2 = MaterialColor(
    _primaryColor2.value, getMaterialCustomColor(_primaryColor2.value));

var projectColorDark = MaterialColor(
    _primaryColorDark.value, getMaterialCustomColor(_primaryColorDark.value));

int selectedItemPosition = 0;
int initialPage = 0;
List<Riddles> favorites = [];
List<Riddles> riddleList = [];

List<General> categoryList = const <General>[
  General(name: "Klasik", url: "1.gif"),
  General(name: "İlginç", url: "2.gif"),
  General(name: "Çocuk", url: "3.gif"),
  General(name: "Komik", url: "4.gif"),
  General(name: "Saçma", url: "5.gif"),
  General(name: "Şaşırtıcı", url: "6.gif"),
  General(name: "Zor", url: "7.gif"),
  General(name: "Manili", url: "8.gif"),
  General(name: "Zeka", url: "9.gif"),
];
