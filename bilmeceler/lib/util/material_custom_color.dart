import 'dart:ui';

Map<int, Color> getMaterialCustomColor(int _color){
  return {
    50:Color(_color),
    100:Color(_color),
    200:Color(_color),
    300:Color(_color),
    400:Color(_color),
    500:Color(_color),
    600:Color(_color),
    700:Color(_color),
    800:Color(_color),
    900:Color(_color),
  };
}