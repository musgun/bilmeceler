import 'package:amenerrasulu/util/constant.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:url_launcher/url_launcher.dart';

import 'detail_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool showSelectedLabels = false;
  bool showUnselectedLabels = false;

  late InterstitialAd _admobInterstitial;
  late BannerAd _admobBanner;

  bool _admobBannerIsAdLoaded = false,
      _admobInterstitialIsLoaded = false,
      _admobInterstitialIsLoading = false;

  int interstitialShowCount = 2;
  int interstitialCurrentCount = 0;


  @override
  void initState() {
    super.initState();
    loadAdmobInterstitial();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void loadAdmobInterstitial() {
    if (!_admobInterstitialIsLoading) {
      setState(() {
        _admobInterstitialIsLoading = true;
      });

      InterstitialAd.load(
          adUnitId: admobInterstitialId,
          request: AdRequest(),
          adLoadCallback: InterstitialAdLoadCallback(
            onAdLoaded: (InterstitialAd ad) {
              _admobInterstitial = ad;
              setState(() {
                _admobInterstitialIsLoaded = true;
              });
            },
            onAdFailedToLoad: (LoadAdError error) {
              setState(() {
                _admobInterstitialIsLoaded = false;
                _admobInterstitialIsLoading = false;
              });

              Future.delayed(const Duration(seconds: 30), () {
                loadAdmobInterstitial();
              });
            },
          ));
    }
  }

  void showAdmobInterstitial() {
    interstitialCurrentCount++;
    if (interstitialCurrentCount >= interstitialShowCount) {
      if (_admobInterstitialIsLoaded) {
        _admobInterstitial.show();

        interstitialCurrentCount = 0;
        interstitialShowCount *= 2;

        setState(() {
          _admobInterstitialIsLoaded = false;
          _admobInterstitialIsLoading = false;
        });

        Future.delayed(const Duration(seconds: 3), () {
          loadAdmobInterstitial();
        });
      }
    }
    if (!_admobInterstitialIsLoaded && !_admobInterstitialIsLoading) {
      loadAdmobInterstitial();
    }
  }

  void loadAdmobBanners() {
    MobileAds.instance.initialize();

    _admobBanner = BannerAd(
      adUnitId: admobBannerId,
      size: AdSize.fullBanner,
      request: AdRequest(),
      listener: BannerAdListener(
        onAdLoaded: (_) {
          setState(() {
            _admobBannerIsAdLoaded = true;
          });
        },
        onAdFailedToLoad: (ad, error) {
          // Releases an ad resource when it fails to load
          ad.dispose();

          print('Ad load failed (code=${error.code} message=${error.message})');
        },
      ),
    );
    _admobBanner.load();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: projectColor, //Color(0xff231F20),
        appBar: AppBar(
          automaticallyImplyLeading: false,
          centerTitle: true,
          title: Text(
            projectName,
            style:
                GoogleFonts.ubuntu(color: projectColorDark, fontSize: fontSize),
          ),
          actions: [
            IconButton(
              icon: Image.asset(
                "assets/images/like.png",
              ),
              onPressed: () {
                _launchURL(url);
              },
            ),
          ],
          backgroundColor: projectColor2,
        ),
        body: renderBody());
  }

  Widget renderBody() {
    return categoryList.isEmpty
        ? Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Center(
                  child: Text(
                    "Kayıt bulunamadı.",
                    style: GoogleFonts.ubuntu(fontSize: fontSize),
                  ),
                ),
              )
            ],
          )
        : AnimationLimiter(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                child: GridView.count(
                  shrinkWrap: true,
                  controller: ScrollController(),
                  crossAxisCount: 3,
                  children: List.generate(
                    categoryList.length,
                    (int index) {
                      return AnimationConfiguration.staggeredGrid(
                        position: index,
                        duration: const Duration(milliseconds: 400),
                        columnCount: categoryList.length,
                        child: ScaleAnimation(
                          child: FadeInAnimation(
                            child: GestureDetector(
                              onTap: () {
                                showAdmobInterstitial();
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => DetailScreen(
                                              title: categoryList[index].name,
                                              index: index,
                                            )));
                              },
                              child: Card(
                                color:const Color(0xffFCFCFC),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(4.0),
                                ),
                                child: Stack(children: <Widget>[
                                  Center(
                                    child: Image.asset(
                                      "assets/images/" +
                                          categoryList[index].url,
                                      height: 75,
                                    ),
                                  ),
                                  Column(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Container(
                                            width: double.infinity,
                                            padding: const EdgeInsets.fromLTRB(
                                                0, 0, 0, 8),
                                            child: AutoSizeText(
                                              categoryList[index].name,
                                              minFontSize: 14,
                                              maxFontSize: 16,
                                              overflow: TextOverflow.ellipsis,
                                              style: GoogleFonts.ubuntu(
                                                shadows: <Shadow>[
                                                  const Shadow(
                                                    offset: Offset(1, 1),
                                                    blurRadius: 1.0,
                                                    color: Color.fromARGB(
                                                        255, 0, 0, 0),
                                                  ),
                                                ],
                                              ),
                                              textAlign: TextAlign.center,
                                              maxLines: 1,
                                            )),
                                      ]),
                                ]),
                              ),
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
            ),
          );
  }

  void _launchURL(val) async {
    // ignore: deprecated_member_use
    if (!await launch(val)) throw 'Url bulunamadı';
  }
}
