import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slimy_card/flutter_slimy_card.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import '../service/sqlite_service.dart';
import '../util/constant.dart';

class FavoriteListScreen extends StatefulWidget {
  const FavoriteListScreen({Key? key}) : super(key: key);

  @override
  State<FavoriteListScreen> createState() => _FavoriteListScreenState();
}

class _FavoriteListScreenState extends State<FavoriteListScreen>
    with TickerProviderStateMixin {
  bool _admobBannerIsAdLoaded = false;
  late BannerAd _admobBanner;

  @override
  void initState() {
    super.initState();
    loadAdmobBanners();
  }

  void loadAdmobBanners() {
    MobileAds.instance.initialize();

    _admobBanner = BannerAd(
      adUnitId: admobBannerId,
      size: AdSize.fullBanner,
      request: AdRequest(),
      listener: BannerAdListener(
        onAdLoaded: (_) {
          setState(() {
            _admobBannerIsAdLoaded = true;
          });
        },
        onAdFailedToLoad: (ad, error) {
          // Releases an ad resource when it fails to load
          ad.dispose();

          print('Ad load failed (code=${error.code} message=${error.message})');
        },
      ),
    );
    _admobBanner.load();
  }

  _updateFavorite(id) async {
    if (favorites.where((element) => element.id == id).isNotEmpty) {
      SqliteService().deleteFavorite(id).then((y) => {
            setState(() {
              favorites.removeWhere((x) => x.id == id);
            })
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: projectColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          centerTitle: true,
          title: Text(
            "Favorilerim",
            style:
                GoogleFonts.ubuntu(color: projectColorDark, fontSize: fontSize),
          ),
          backgroundColor: projectColor2,
        ),
        body: renderBody());
  }

  Widget renderBody() {
    return favorites.isEmpty
        ? Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Center(
                  child: Text(
                    "Favori bilmeceniz bulunmamaktadır!",
                    style: GoogleFonts.ubuntu(
                        fontSize: fontSize, color: Colors.black),
                  ),
                ),
              )
            ],
          )
        : Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            renderBanner(),
            Expanded(child: SingleChildScrollView(child: renderMessages())),
          ],
        );
  }

  topWidget2(index, id) {
    return SafeArea(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          favoriteAdd(id),
          Flexible(
            child: AutoSizeText(
              favorites[index].question + "?",
              minFontSize: 10,
              maxFontSize: 15,
              textAlign: TextAlign.justify,
              style:
                  GoogleFonts.ubuntu(fontSize: fontSize, color: Colors.black),
            ),
          ),
          const SizedBox(
            height: 25,
          )
        ],
      ),
    );
  }

  bottomWidget2(index) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
      child: AutoSizeText(
        favorites[index].answer,
        minFontSize: 10,
        maxFontSize: 15,
        textAlign: TextAlign.justify,
        style: GoogleFonts.ubuntu(
            fontSize: fontSize,
            fontWeight: FontWeight.bold,
            color: Colors.black),
      ),
    );
  }

  Widget renderMessages() {
    return AnimationLimiter(
      child: ListView.builder(
          itemCount: favorites.length,
          shrinkWrap: true,
          primary: true,
          physics: const NeverScrollableScrollPhysics(),
          itemBuilder: (BuildContext context, int index) {
            return AnimationConfiguration.staggeredGrid(
              position: index,
              duration: const Duration(milliseconds: 400),
              columnCount: favorites.length,
              child: ScaleAnimation(
                child: FadeInAnimation(
                  child: GestureDetector(
                    onTap: () {},
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(10, 2, 10, 2),
                      child: Card(
                        shape: Border(
                            left: BorderSide(color: projectColor2, width: 5)),
                        child: ExpansionTile(
                          trailing: favoriteAdd(favorites[index].id),
                          collapsedTextColor: projectColorDark,
                          textColor: projectColorDark,
                          iconColor: projectColorDark,
                          backgroundColor: projectColor2,
                          title: Padding(
                            padding: const EdgeInsets.fromLTRB(5, 10, 5, 10),
                            child: Text(
                              favorites[index].question + "?",
                              textAlign: TextAlign.justify,
                              style: GoogleFonts.ubuntu(
                                  fontSize: fontSize, height: 1.5),
                            ),
                          ),
                          children: <Widget>[
                            Divider(),
                            ListTile(
                              title: Center(
                                child: Text(
                                  favorites[index].answer,
                                  style: GoogleFonts.ubuntu(
                                      fontWeight: FontWeight.w700,
                                      height: 2,
                                      fontSize: fontSize),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),

                    /* Padding(
                      padding: const EdgeInsets.fromLTRB(5, 0, 5, 1),
                      child: FlutterSlimyCard(
                        borderRadius: 3,
                        topCardHeight: 150,
                        color: Color(0xffF4C633), //const Color(0xffF4C632),
                        cardWidth: screenWidth,
                        topCardWidget: topWidget2(index, favorites[index].id),
                        bottomCardWidget: bottomWidget2(index),
                      ),
                    ),*/
                  ),
                ),
              ),
            );
          }),
    );
  }

  Widget favoriteAdd(id) {
    return IconButton(
        icon: Icon(
          favorites.where((x) => x.id == id).isNotEmpty
              ? Icons.star
              : Icons.star_border_outlined,
          color: favorites.where((x) => x.id == id).isNotEmpty
              ? projectColorDark
              : projectColorDark,
          size: 22,
        ),
        onPressed: () {
          _updateFavorite(id);
        });
  }
  Widget renderBanner() {
    return _admobBannerIsAdLoaded
        ? Container(
      child: AdWidget(ad: _admobBanner),
      width: _admobBanner.size.width.toDouble(),
      height: _admobBanner.size.height.toDouble(),
      alignment: Alignment.center,
    )
        : const SizedBox();
  }

}
