import 'dart:math';

import 'package:amenerrasulu/main.dart';
import 'package:amenerrasulu/util/constant.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:card_swiper/card_swiper.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';

import '../component/share_component.dart';
import '../service/sqlite_service.dart';

class DetailScreen extends StatefulWidget {
  final int index;
  final String title;
  final bool? isFavorite;

  const DetailScreen(
      {Key? key, required this.title, required this.index, this.isFavorite})
      : super(key: key);

  @override
  State<DetailScreen> createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  late InterstitialAd _admobInterstitial;
  late BannerAd _admobBanner;

  bool _admobBannerIsAdLoaded = false,
      _admobInterstitialIsLoaded = false,
      _admobInterstitialIsLoading = false;

  int interstitialShowCount = 3;
  int interstitialCurrentCount = 0;

  bool isLoading = true;
  int selectedIndex = 0;

  @override
  void initState() {
    super.initState();
    loadAdmobBanners();
    loadAdmobInterstitial();
    _getRiddles();

  }

  @override
  void dispose() {
    super.dispose();
  }

  _getRiddles() async {
    selectedIndex = await sPrefHelper.getBookmark(widget.title);

    SqliteService()
        .getRiddlesByCategory(widget.title)
        .then((value) => setState(() {
              isLoading = false;
              riddleList = value;
            }));
  }

  void loadAdmobBanners() {
    MobileAds.instance.initialize();

    _admobBanner = BannerAd(
      adUnitId: admobBannerId,
      size: AdSize.fullBanner,
      request: AdRequest(),
      listener: BannerAdListener(
        onAdLoaded: (_) {
          setState(() {
            _admobBannerIsAdLoaded = true;
          });
        },
        onAdFailedToLoad: (ad, error) {
          // Releases an ad resource when it fails to load
          ad.dispose();
        },
      ),
    );
    _admobBanner.load();
  }

  void loadAdmobInterstitial() {
    if (!_admobInterstitialIsLoading) {
      setState(() {
        _admobInterstitialIsLoading = true;
      });

      InterstitialAd.load(
          adUnitId: admobInterstitialId,
          request: AdRequest(),
          adLoadCallback: InterstitialAdLoadCallback(
            onAdLoaded: (InterstitialAd ad) {
              _admobInterstitial = ad;
              setState(() {
                _admobInterstitialIsLoaded = true;
              });
            },
            onAdFailedToLoad: (LoadAdError error) {
              setState(() {
                _admobInterstitialIsLoaded = false;
                _admobInterstitialIsLoading = false;
              });

              Future.delayed(const Duration(seconds: 30), () {
                loadAdmobInterstitial();
              });
            },
          ));
    }
  }

  void showAdmobInterstitial() {
    interstitialCurrentCount++;
    if (interstitialCurrentCount >= interstitialShowCount) {
      if (_admobInterstitialIsLoaded) {
        _admobInterstitial.show();

        interstitialCurrentCount = 0;
        interstitialShowCount *= 2;

        setState(() {
          _admobInterstitialIsLoaded = false;
          _admobInterstitialIsLoading = false;
        });

        Future.delayed(const Duration(seconds: 3), () {
          loadAdmobInterstitial();
        });
      }
    }
    if (!_admobInterstitialIsLoaded && !_admobInterstitialIsLoading) {
      loadAdmobInterstitial();
    }
  }

  _updateFavorite() async {
    if (favorites
        .where((element) => element.id == riddleList[selectedIndex].id)
        .isEmpty) {
      SqliteService().addFavorite(riddleList[selectedIndex].id).then((y) => {
            setState(() {
              favorites.add(riddleList[selectedIndex]);
            })
          });
    } else {
      SqliteService().deleteFavorite(riddleList[selectedIndex].id).then((y) => {
            setState(() {
              favorites
                  .removeWhere((x) => x.id == riddleList[selectedIndex].id);
            })
          });
    }
  }

  Future<void> shareFile(text) async {
    ShareComponent().shareContent(text, "Bilmeceler");
  }

  void _random() {
    setState(() {
      Random random = Random();
      int randomNumber = random.nextInt(riddleList.length - 1);
      selectedIndex = randomNumber;
      sPrefHelper.setBookmark(widget.title, selectedIndex);
    });
    showAdmobInterstitial();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: projectColor2,
      appBar: AppBar(
        iconTheme: const IconThemeData(
          color: Colors.black,
        ),
        actions: (isLoading || riddleList.isEmpty)
            ? []
            : [
                IconButton(
                    icon: const Icon(
                      Icons.wifi_protected_setup,
                      color: Colors.black,
                      size: 19,
                    ),
                    tooltip: "Rastgele",
                    onPressed: () {
                      _random();
                    }),
                IconButton(
                    icon: const Icon(
                      Icons.share,
                      color: Colors.black,
                      size: 19,
                    ),
                    tooltip: "Paylaş",
                    onPressed: () {
                      shareFile(riddleList[selectedIndex].question +
                          "?\n\n" +
                          "Cevap: " +
                          riddleList[selectedIndex].answer);
                    }),
                favoriteAdd()
              ],
        excludeHeaderSemantics: true,
        backgroundColor: projectColor2, // const Color(0xffDBAF2E),
        centerTitle: true,
        title: Text(
          riddleList.length.toString() + "/" + (selectedIndex + 1).toString(),
          style:
              GoogleFonts.ubuntu(fontSize: fontSize, color: projectColorDark),
        ),
      ),
      body: isLoading
          ? Container(
              width: double.infinity,
              height: 500,
              alignment: Alignment.center,
              child: CircularProgressIndicator(
                color: projectColor2,
              ),
            )
          : Swiper(
              index: selectedIndex,
              itemBuilder: (BuildContext context, int index) {
                return Column(
                  children: [          renderBanner(),

    _renderSlimyCard()],
                );
              },
              itemCount: riddleList.length,
              onIndexChanged: (index) {
                showAdmobInterstitial();
                setState(() {
                  selectedIndex = index;
                });

                sPrefHelper.setBookmark(widget.title, index);
              },
              control: SwiperControl(color: Colors.black, size: 13),
            ),
    );
  }

  _renderSlimyCard() {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            flex: 2,
            child: _renderContent(context),
          ),
          Center(
              child: Text(
            "Cevap için beyaz alana tıklayınız..",
            style:
                GoogleFonts.ubuntu(fontSize: 11, fontStyle: FontStyle.italic),
          ))
        ],
      ),
    );
  }

  _renderContent(context) {
    double size = MediaQuery.of(context).size.height;
    return Stack(
      children: [
        Image.asset(
          "assets/images/bg.png",
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          fit: BoxFit.cover,
        ),
        Card(
          elevation: 0.0,
          margin: const EdgeInsets.only(
              left: 32.0, right: 32.0, top: 35, bottom: 35),
          color: const Color(0x00000000),
          child: FlipCard(
            fill: Fill.fillBack,
            onFlip: () {},
            direction: FlipDirection.HORIZONTAL,
            speed: 1000,
            onFlipDone: (status) {},
            front: Container(
              decoration: BoxDecoration(
                color: projectColor,
                borderRadius: const BorderRadius.all(Radius.circular(4.0)),
              ),
              child: Center(
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        "assets/images/11.gif",
                        height: 60,
                      ),
                     const SizedBox(
                        height: 15,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(10),
                        child: AutoSizeText(
                          riddleList[selectedIndex].question + "?",
                          textAlign: TextAlign.justify,
                          minFontSize: 14,
                          maxFontSize: 18,
                          style: GoogleFonts.ubuntu(fontSize: 16, height: 2),
                        ),
                      ),
                      SizedBox(
                        height: (size * 0.12),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            back: Container(
              decoration: BoxDecoration(
                color: projectColorDark,
                borderRadius: const BorderRadius.all(Radius.circular(10)),
              ),
              child: Center(
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: AutoSizeText(
                          riddleList[selectedIndex].answer,
                          textAlign: TextAlign.center,
                          minFontSize: 14,
                          maxFontSize: 18,
                          style: GoogleFonts.ubuntu(
                            fontSize: 16,
                            height: 2,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget renderBanner() {
    return _admobBannerIsAdLoaded
        ? Container(
      child: AdWidget(ad: _admobBanner),
      width: _admobBanner.size.width.toDouble(),
      height: _admobBanner.size.height.toDouble(),
      alignment: Alignment.center,
    )
        : const SizedBox();
  }

  Widget favoriteAdd() {
    return IconButton(
        icon: Icon(
          favorites
              .where((x) => x.id == riddleList[selectedIndex].id)
              .isNotEmpty
              ? Icons.star
              : Icons.star_border_outlined,
          color: favorites
              .where((x) => x.id == riddleList[selectedIndex].id)
              .isNotEmpty
              ? const Color(0xffE61900)
              : Colors.black,
          size: 22,
        ),
        onPressed: () {
          _updateFavorite();
        });
  }
}
