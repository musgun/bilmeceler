import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesHelper {
  SharedPreferences? _prefs;

  _init() async {
    _prefs = await SharedPreferences.getInstance();
  }

  setBookmark(String category, int index) async {
    if (_prefs == null) {
      await _init();
    }
    await _prefs!.setInt(category, index);
  }

  Future<int> getBookmark(String category) async {
    if (_prefs == null) {
      await _init();
    }
    return _prefs!.getInt(category) ?? 0;
  }
}
