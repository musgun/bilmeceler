import 'package:share/share.dart';

class ShareComponent {
  void shareContent(String fileOrText, String titleText) async {
    Share.share(fileOrText, subject: titleText);
  }
}
