class General {
  final int? count;
  final String name;
  final String url;

  const General({
    this.count,
    required this.name,
    required this.url,
  });
}

class Riddles {
  final int id;
  final String category;
  final String question;
  final String answer;
  final int favorite;

  const Riddles({
    required this.id,
    required this.category,
    required this.question,
    required this.answer,
    required this.favorite,
  });
}

class Stain {
  final String title;
  final String? need;
  final String? warning;
  final String? steps;

  const Stain({
    required this.title,
    this.need,
    this.warning,
    this.steps,
  });
}

class Stains {
  final String name;
  final String img;
  final List<Stain> stain;

  const Stains({
    required this.name,
    required this.img,
    required this.stain,
  });
}
