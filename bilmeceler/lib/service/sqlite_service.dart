import 'package:flutter/services.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:io' as io;

import '../models/general_model.dart';

class SqliteService {
  Future<Database> initializeDB() async {
    io.Directory applicationDirectory =
        await getApplicationDocumentsDirectory();

    String dbPathEnglish = path.join(applicationDirectory.path, "t.ttf");

    bool dbExistsEnglish = await io.File(dbPathEnglish).exists();

    if (!dbExistsEnglish) {
      // Copy from asset
      ByteData data = await rootBundle.load(path.join("assets/fonts", "t.ttf"));
      List<int> bytes =
          data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);

      // Write and flush the bytes written
      await io.File(dbPathEnglish).writeAsBytes(bytes, flush: true);
    }

    return await openDatabase(dbPathEnglish);
  }

  Future<List<Riddles>> getRiddlesByCategory(category) async {
    final db = await initializeDB();

    final List<Map<dynamic, dynamic>> maps = await db
        .rawQuery('SELECT *  FROM riddles where category=?', [category]);

    return List.generate(maps.length, (i) {
      return Riddles(
        id: maps[i]['id'],
        favorite: maps[i]['favorite'],
        category: maps[i]['category'],
        answer: maps[i]['answer'],
        question: maps[i]['question'],
      );
    });
  }

  Future addFavorite(id) async {
    final db = await initializeDB();
    await db.rawQuery("update riddles set favorite = '1' where id=?", [id]);
  }

  Future deleteFavorite(id) async {
    final db = await initializeDB();
    await db.rawQuery("update riddles set favorite = '0' where id=?", [id]);
  }

  Future<List<Riddles>> getFavorites() async {
    final db = await initializeDB();

    final List<Map<String, dynamic>> maps = await db
        .rawQuery("SELECT * FROM riddles where favorite='1' order by id");

    return List.generate(maps.length, (i) {
      return Riddles(
        id: maps[i]['id'],
        favorite: maps[i]['favorite'],
        category: maps[i]['category'],
        answer: maps[i]['answer'],
        question: maps[i]['question'],
      );
    });
  }
}
