import 'package:amenerrasulu/screen/favori_list_screen.dart';
import 'package:amenerrasulu/util/constant.dart';
import 'package:flutter_snake_navigationbar/flutter_snake_navigationbar.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'component/shared_preferences_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:splash_screen_view/SplashScreenView.dart';
import 'screen/home_screen.dart';
import 'service/sqlite_service.dart';

var sPrefHelper = SharedPreferencesHelper();

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  MobileAds.instance.initialize();
  runApp(const MyApp());
}

final ThemeData lightTheme = ThemeData(
    visualDensity: VisualDensity.adaptivePlatformDensity,
    brightness: Brightness.light,
    primaryColor: projectColor,
    primaryColorLight: projectColor,
    backgroundColor: projectColor);

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget splash = SplashScreenView(
      navigateRoute: const MyHomePage(),
      duration: 2500,
      imageSize: 85,
      imageSrc: "assets/images/4.gif",
      text: projectName,
      textType: TextType.ColorizeAnimationText,
      colors: [projectColorDark, projectColor2, projectColorDark],
      textStyle:
          GoogleFonts.ubuntu(fontSize: fontSize, fontWeight: FontWeight.bold),
    );

    return GetMaterialApp(
      theme: lightTheme,
      debugShowCheckedModeBanner: false,
      title: projectName,
      home: isSplash == false ? splash : const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool showSelectedLabels = false;
  bool showUnselectedLabels = false;
  var pageController = PageController();

  @override
  void initState() {
    super.initState();
    pageController = PageController(initialPage: initialPage);
    _customInit();
    OneSignal.shared.setAppId(oneSignalId);
    OneSignal.shared.promptUserForPushNotificationPermission();
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual,
        overlays: [SystemUiOverlay.bottom]);
    isSplash = true;
  }

  _customInit() async {
    if (favorites.isEmpty) {
      SqliteService().getFavorites().then((value) => favorites = value);
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Builder(builder: (context) {
      return SafeArea(
        child: Scaffold(
          backgroundColor: projectColor,
          body: PageView(
            onPageChanged: (index) {
              setState(() => {
                    selectedItemPosition = index,
                  });
            },
            controller: pageController,
            children: const <Widget>[
              HomeScreen(),
              FavoriteListScreen(),
            ],
          ),
          bottomNavigationBar: SnakeNavigationBar.color(
            behaviour: SnakeBarBehaviour.floating,
            snakeShape: SnakeShape.rectangle,
            height: 42,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
            padding: const EdgeInsets.all(13),

            snakeViewColor: const Color(0xff231F20),
            //Color(0xffDBAF2E), //Color(0xffDBAF2E),
            selectedItemColor: projectColor,
            unselectedItemColor: projectColor,
            //Color(0xffDBAF2E),
            backgroundColor: projectColor2, //const Color(0xffF4C633),
            //Color(0xff231F20),
            showUnselectedLabels: showUnselectedLabels,
            showSelectedLabels: showSelectedLabels,
            currentIndex: selectedItemPosition,
            onTap: (index) {
              setState(() => {
                    selectedItemPosition = index,
                    pageController.jumpToPage(index)
                  });
            },
            items: const [
              BottomNavigationBarItem(
                  icon: Icon(
                    Icons.home,
                    size: 22,
                  ),
                  label: 'Home'),
              BottomNavigationBarItem(
                  icon: Icon(
                    Icons.star,
                    size: 21,
                  ),
                  label: 'Favorite'),
            ],
            selectedLabelStyle: GoogleFonts.ubuntu(fontSize: fontSize - 6),
            unselectedLabelStyle: GoogleFonts.ubuntu(fontSize: fontSize - 10),
          ),
        ),
      );
    });
  }
}
